/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csb.controller;

import static csb.CSB_PropertyType.REMOVE_ITEM_MESSAGE;
import csb.data.Course;
import csb.data.CourseDataManager;
import csb.data.Assignment;
import csb.gui.CSB_GUI;
import csb.gui.MessageDialog;
import csb.gui.AssignmentItemDialog;
import csb.gui.YesNoCancelDialog;
import java.util.Collections;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author Matthew Wong
 */
public class AssignmentEditController {
    AssignmentItemDialog aid;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    
    
    //CONSTRUCTOR
    public AssignmentEditController(Stage initPrimaryStage, Course course, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        aid = new AssignmentItemDialog(initPrimaryStage, course, initMessageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
    }
    
    //ADD AN ASSIGNMENT
    public void handleAddAssignmentRequest(CSB_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        aid.showAddAssignmentDialog(course.getStartingMonday());
        
        if (aid.wasCompleteSelected()) {
            //IF THE USER CONFIRMED, GET THE ASSIGNMENT
            Assignment ai = aid.getAssignment();
            
            //AND ADD IT TO THE TABLE
            course.addAssignment(ai);
        }
    }
    
    //EDIT AN ASSIGNMENT
    public void handleEditAssignmentRequest(CSB_GUI gui, Assignment itemToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        aid.showEditAssignmentDialog(itemToEdit);
        
        if (aid.wasCompleteSelected()) {
            //IF THE SUER CONFIRMED, UPDATE THE ASSIGNMENT
            Assignment ai = aid.getAssignment();
            itemToEdit.setName(ai.getName());
            itemToEdit.setTopics(ai.getTopics());
            itemToEdit.setDate(ai.getDate());
            Collections.sort(gui.getDataManager().getCourse().getAssignments());
        }
    }
    
    //REMOVE AN ASSIGNMENT
    public void handleRemoveAssignmentRequest(CSB_GUI gui, Assignment itemToRemove) {
        //PROMPT TO REMOVE (CONFIRM)
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        //GET SELECTION
        String selection = yesNoCancelDialog.getSelection();
        if (selection.equals(YesNoCancelDialog.YES)) {
            gui.getDataManager().getCourse().removeAssignment(itemToRemove);
        }
    }
}
