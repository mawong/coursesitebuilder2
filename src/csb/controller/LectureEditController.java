/*
 * This entire class handles actions from the lecture schedule. You can add, edit, or remove
 * the lectures as you please.
 */
package csb.controller;

import static csb.CSB_PropertyType.REMOVE_ITEM_MESSAGE;
import csb.data.Course;
import csb.data.CourseDataManager;
import csb.data.Lecture;
import csb.gui.CSB_GUI;
import csb.gui.MessageDialog;
import csb.gui.LectureItemDialog;
import csb.gui.YesNoCancelDialog;
import java.util.Collections;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author Matthew Wong (109023296)
 */
public class LectureEditController {

    LectureItemDialog lid;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;

    //CONSTRUCTOR
    public LectureEditController(Stage initPrimaryStage, Course course, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        lid = new LectureItemDialog(initPrimaryStage, course, initMessageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
    }

    //ADD A LECTURE
    public void handleAddLectureRequest(CSB_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        lid.showAddLectureDialog();

        if (lid.wasCompleteSelected()) {
            //IF THE USER CONFIRMED, GET THE LECTURE
            Lecture li = lid.getLecture();
            li.setSessions(lid.getSessionsComboBoxValue());

            //AND ADD IT TO THE TABLE
            course.addLecture(li);
        }
    }
    
    //EDIT A LECTURE
    public void handleEditLectureRequest(CSB_GUI gui, Lecture itemToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        lid.showEditLectureDialog(itemToEdit);
        
        if (lid.wasCompleteSelected()) {
            //IF THE USER CONFIRMED, UPDATE THE LECTURE
            Lecture li = lid.getLecture();
            itemToEdit.setTopic(li.getTopic());
            itemToEdit.setSessions(li.getSessions());
        }
    }
    
    //REMOVE A LECTURE
    public void handleRemoveLectureRequest(CSB_GUI gui, Lecture itemToRemove) {
        //PROMPT TO REMOVE (CONFIRM)
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        //GET SELECTION
        String selection =  yesNoCancelDialog.getSelection();
        
        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getCourse().removeLecture(itemToRemove);
        }
    }
    
    //MOVE LECTURE UP
    public void handleMoveLectureUpRequest(CSB_GUI gui, Lecture itemToMove, TableView<Lecture> lecture) {
        ObservableList<Lecture> lectures = gui.getDataManager().getCourse().getLectures();
        ObservableList<Lecture> newLectures = FXCollections.observableArrayList();
        int count = 0; //used for getting the amuont of lectures before the one to move
        
        //FIND THE LECTURE IN THE LIST
        for (int i = 0; i < lectures.size(); i++) {
            if (lectures.get(i).topicProperty().equals(itemToMove.topicProperty())) {
                //SINCE WE GOT THE CORRECT LECTURE, SAVE IT IN COUNT
                count = i;
                break;
            }
        }
        
        Collections.swap(lectures, count, count - 1);
    }
    
    //MOVE LECTURE DOWN
    public void handleMoveLectureDownRequest(CSB_GUI gui, Lecture itemToMove, TableView<Lecture> lecture) {
        ObservableList<Lecture> lectures = gui.getDataManager().getCourse().getLectures();
        ObservableList<Lecture> newLectures = FXCollections.observableArrayList();
        int count = 0; //used for getting the amuont of lectures before the one to move
        
        //FIND THE LECTURE IN THE LIST
        for (int i = 0; i < lectures.size(); i++) {
            if (lectures.get(i).topicProperty().equals(itemToMove.topicProperty())) {
                //SINCE WE GOT THE CORRECT LECTURE, SAVE IT IN COUNT
                count = i;
                break;
            }
        }
        
        Collections.swap(lectures, count, count + 1);
    }
}
