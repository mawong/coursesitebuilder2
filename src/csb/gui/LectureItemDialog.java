/*
 * Handles the dialog box to add or edit a lecture.
 */
package csb.gui;

import csb.data.Course;
import csb.data.Lecture;
import static csb.gui.CSB_GUI.CLASS_HEADING_LABEL;
import static csb.gui.CSB_GUI.CLASS_PROMPT_LABEL;
import static csb.gui.CSB_GUI.PRIMARY_STYLE_SHEET;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
/**
 *
 * @author Matthew Wong (109023296)
 */
public class LectureItemDialog extends Stage {
    //OBJECT DATA
    Lecture lecture;
    
    //GUI CONTROLS FOR THE DIALOG
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label topicsLabel;
    TextField topicsTextField;
    Label sessionsLabel;
    ComboBox sessionsComboBox;
    Button completeButton;
    Button cancelButton;
    
    //KEEP TRACK OF WHICH BUTTON IS PRESSED
    String selection;
    
    //CONSTANTS
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String TOPICS_PROMPT = "Topic: ";
    public static final String SESSIONS_PROMPT = "Number of Sessions: ";
    public static final String LECTURE_HEADING = "Lecture Details";
    public static final String ADD_LECTURE_TITLE = "Add New Lecture";
    public static final String EDIT_LECTURE_TITLE = "Edit Lecture";
    
    /**
     * Initializes this dialog so the user and add or edit a lecture.
     * 
     * @param primaryStage The owner of this dialog.
     */
    public LectureItemDialog(Stage primaryStage, Course course, MessageDialog messageDialog) {
        // MAKE THIS DIALOG, SO OTHERS WILL WAIT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        //LOAD CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        //PUT THE HEADING ON THE GRID
        headingLabel = new Label(LECTURE_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        
        //ADD THE TOPIC
        topicsLabel = new Label(TOPICS_PROMPT);
        topicsLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        topicsTextField = new TextField();
        topicsTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            lecture.setTopic(newValue);
        });
        
        //AND THE SESSIONS
        sessionsLabel = new Label(SESSIONS_PROMPT);
        sessionsLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        sessionsComboBox  = initComboBox();
        ObservableList<Integer> sessionsIntegers = FXCollections.observableArrayList();
        for (int i = 1; i <= 3; i++) {
            sessionsIntegers.add(i);
        }
        sessionsComboBox.setItems(sessionsIntegers);
        sessionsComboBox.setOnAction(e -> {
            lecture.setSessions(getSessionsComboBoxValue());
        });
        
        //AND THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        //ADD EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            LectureItemDialog.this.selection = sourceButton.getText();
            LectureItemDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);
        
        //ARRANGE THEM IN THE PANE
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(topicsLabel, 0, 1, 1, 1);
        gridPane.add(topicsTextField, 1, 1, 1, 1);
        gridPane.add(sessionsLabel, 0, 2, 1, 1);
        gridPane.add(sessionsComboBox, 1, 2, 1, 1);
        gridPane.add(completeButton, 0, 3, 1, 1);
        gridPane.add(cancelButton, 1, 3, 1, 1);
        
        //PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    /**
     * Gets the selection from the user.
     * 
     * @return YES, NO, or CANCEL (depends on which button the user selected)
     */
    public String getSelection() {
        return selection;
    }
    
    public Lecture getLecture() { 
        return lecture;
    }
    
    /**
     * Loads a lecture
     */
    public Lecture showAddLectureDialog() {
        // SET THE DIALOG TITLE
        setTitle(ADD_LECTURE_TITLE);
        
        // RESET THE LECTURE OBJECT WITH DEFAULT VALUES
        lecture = new Lecture();
        
        // LOAD THE UI STUFF
        topicsTextField.setText(lecture.getTopic());
        sessionsComboBox.setValue(lecture.getSessions());
        
        // AND OPEN IT UP
        this.showAndWait();
        
        return lecture;
    }
    
    //LOAD THE GUI DATA
    public void loadGuiData() {
        topicsTextField.setText(lecture.getTopic());
        sessionsComboBox.setValue(lecture.getSessions());
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    public void showEditLectureDialog(Lecture itemToEdit) {
        // SET THE TITLE
        setTitle(EDIT_LECTURE_TITLE);
        
        // LOAD THE LECTURE INTO OUR LOCAL OBJECT
        lecture = new Lecture();
        lecture.setTopic(itemToEdit.getTopic());
        lecture.setSessions(itemToEdit.getSessions());
        
        // AND THEN INTO OUR GUI
        loadGuiData();
               
        // AND OPEN IT UP
        this.showAndWait();
    }
    
    //INITIALIZE THE COMBO BOX
    private ComboBox initComboBox() {
        ComboBox comboBox = new ComboBox();
        return comboBox;
    }
    
    public int getSessionsComboBoxValue() {
        return sessionsComboBox.getSelectionModel().getSelectedIndex() + 1;
    }
    
    public ComboBox getComboBox() {
        return sessionsComboBox;
    }
}
