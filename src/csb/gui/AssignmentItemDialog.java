/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csb.gui;

import csb.CSB_PropertyType;
import csb.data.Course;
import csb.data.Assignment;
import static csb.gui.CSB_GUI.CLASS_HEADING_LABEL;
import static csb.gui.CSB_GUI.CLASS_PROMPT_LABEL;
import static csb.gui.CSB_GUI.PRIMARY_STYLE_SHEET;
import java.time.LocalDate;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author Matthew Wong
 */
public class AssignmentItemDialog extends Stage {
    //OBJECT DATA
    Assignment assignment;
    
    //GUI CONTROLS FOR DIALOG
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label nameLabel;
    TextField nameTextField;
    Label topicsLabel;
    TextField topicsTextField;
    Label dateLabel;
    DatePicker datePicker;
    Button completeButton;
    Button cancelButton;
    
    //KEEP TRACK OF WHICH BUTTON IS PRESSED
    String selection;
    
    //CONSTANTS
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String NAME_PROMPT = "Name: ";
    public static final String TOPICS_PROMPT = "Topics: ";
    public static final String DATE_PROMPT = "Date: ";
    public static final String ASSIGNMENT_HEADING = "Assignment Details";
    public static final String ADD_ASSIGNMENT_TITLE = "Add New Assignment";
    public static final String EDIT_ASSIGNMENT_TITLE = "Edit Assignment";
    
    /**
     * Initializes this dialog so the user and add or edit a lecture.
     * 
     * @param primaryStage The owner of this dialog.
     */
    public AssignmentItemDialog(Stage primaryStage, Course course, MessageDialog messageDialog) {
        //MAKE THE DIALOG, SO OTHERS WILL WAIT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        //LOAD CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        //PUT THE HEADING ON THE GRID
        headingLabel = new Label(ASSIGNMENT_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        
        //ADD THE NAME
        nameLabel = new Label(NAME_PROMPT);
        nameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        nameTextField = new TextField();
        nameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            assignment.setName(newValue);
        });
        
        //AND THE TOPICS
        topicsLabel = new Label(TOPICS_PROMPT);
        topicsLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        topicsTextField = new TextField();
        topicsTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            assignment.setTopics(newValue);
        });
        
        //AND THE DATE
        dateLabel = new Label(DATE_PROMPT);
        dateLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        datePicker = new DatePicker();
        datePicker.setOnAction(e -> {
            if (datePicker.getValue().isBefore(course.getStartingMonday()) || datePicker.getValue().isAfter(course.getEndingFriday())) {
                //NOTIFY USER
                PropertiesManager props  = PropertiesManager.getPropertiesManager();
                messageDialog.show(props.getProperty(CSB_PropertyType.ILLEGAL_DATE_MESSAGE));
            } else {
                assignment.setDate(datePicker.getValue());
            }
        });
        
        //AND THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        //ADD EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            AssignmentItemDialog.this.selection = sourceButton.getText();
            AssignmentItemDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);
        
        //ARRANGE THEM IN THE PANE
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(nameLabel, 0, 1, 1, 1);
        gridPane.add(nameTextField, 1, 1, 1, 1);
        gridPane.add(topicsLabel, 0, 2, 1, 1);
        gridPane.add(topicsTextField, 1, 2, 1, 1);
        gridPane.add(dateLabel, 0, 3, 1, 1);
        gridPane.add(datePicker, 1, 3, 1, 1);
        gridPane.add(completeButton, 0, 4, 1, 1);
        gridPane.add(cancelButton, 1, 4, 1, 1);
        
        //FINALLY, PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    /**
     * Gets the selection from the user.
     * 
     * @return YES, NO, or CANCEL (depends on which button the user selected)
     */
    public String getSelection() {
        return selection;
    }
    
    public Assignment getAssignment() {
        return assignment;
    }
    
    /**
     * Loads an assignment
     */
    public Assignment showAddAssignmentDialog(LocalDate initDate) {
        //SET THE DIALOG TITLE
        setTitle(ADD_ASSIGNMENT_TITLE);
        
        //RESET THE LECTURE OBJECT WITH DEFAULT VALUES
        assignment = new Assignment();
        
        //LOAD UI STUFF
        nameTextField.setText(assignment.getName());
        topicsTextField.setText(assignment.getTopics());
        datePicker.setValue(initDate);
        
        //AND OPEN IT UP
        this.showAndWait();
        
        return assignment;
    }
    
    public void loadGuiData() {
        //LOAD THE UI STUFF
        nameTextField.setText(assignment.getName());
        topicsTextField.setText(assignment.getTopics());
        datePicker.setValue(assignment.getDate());
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    public void showEditAssignmentDialog(Assignment itemToEdit) {
        //SET THE TITLE
        setTitle(EDIT_ASSIGNMENT_TITLE);
        
        //LOAD THE ASSIGNMENT INTO THE LOCAL OBJECT
        assignment = new Assignment();
        assignment.setName(itemToEdit.getName());
        assignment.setTopics(itemToEdit.getTopics());
        assignment.setDate(itemToEdit.getDate());
        
        //AND THEN INTO OUR GUI
        loadGuiData();
        
        //AND OPEN IT UP
        this.showAndWait();
    }
}
