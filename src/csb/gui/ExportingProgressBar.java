/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csb.gui;

import csb.data.Course;
import csb.data.CoursePage;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
/**
 *
 * @author MAVIRI
 */
public class ExportingProgressBar extends Stage {
    Course course;
    Scene scene;
    Label progressLabel;
    ProgressBar bar;
    ProgressIndicator indicator;
    List<CoursePage> pages;
    ReentrantLock progressLock;
    int numPages;
    
    public ExportingProgressBar(Stage primaryStage, Course courseToLoad) {
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        course = courseToLoad;
    }
    
    public void start(Stage primaryStage) throws Exception {
        progressLock = new ReentrantLock();
        VBox box = new VBox();
        
        HBox toolbar = new HBox();
        bar = new ProgressBar(0);
        indicator = new ProgressIndicator(0);
        toolbar.getChildren().add(bar);
        toolbar.getChildren().add(indicator);
        toolbar.setAlignment(Pos.CENTER);
        
        progressLabel = new Label();
        progressLabel.setFont(new Font("Serif", 20));
        box.getChildren().add(progressLabel);
        box.getChildren().add(toolbar);
        box.setAlignment(Pos.CENTER);
        
        Scene scene = new Scene(box, 450, 150);
        primaryStage.setScene(scene);
        
        pages = course.getPages();
        double numPages = pages.size();
        
        Task<Void> task = new Task<Void>() {
            double perc;
            
            @Override
            protected Void call() throws Exception {
                try {
                    progressLock.lock();
                    for (int i = 0; i <= numPages; i++) {
                        perc = (i + 1)/numPages;
                        String name = pages.get(i).name();
                        
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                bar.setProgress(perc);
                                indicator.setProgress(perc);
                                progressLabel.setText("Exporting " + name + " Completed");
                            }
                        });
                        
                        Thread.sleep(1000);
                    }
                }
                finally {
                    progressLock.unlock();
                }
                return null;
            }
        };
        Thread thread = new Thread(task);
        thread.start();
        
        primaryStage.showAndWait();
    }
}
